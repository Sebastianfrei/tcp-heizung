'use strict';

var net = require('net');
const Networker = require('./networker');

var port = 5493;
var host = '127.0.0.1';
var conn = net.createConnection(port ,host);

conn.on('connect', function() {
	
	let networker = new Networker(conn, (data) => {
    console.log('received:', data.toString());
	});
	networker.init();
	networker.send("{ ZeilenIndex: 1 }");
	
    console.log('connected to server');
	
	  
     });
conn.on('data' , function (){
      console.log("Data received from the server: " , data);
        });
conn.on('error', function(err) {
      console.log('Error in connection:', err);
      });
conn.on('close', function() {
       console.log('connection got closed, will try to reconnect');
         conn.end();
       });
conn.on('end' , function(){
      console.log('Requested an end to the TCP connection');
       });